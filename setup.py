from setuptools import setup

setup(
    name='MDT',
    version='',
    packages=[''],
    url='https://bitbucket.org/hommalab/mdt/src/master/',
    license='MIT',
    author='morten',
    author_email='morten.bertz@aoni.waseda.jp',
    description='open MDT files',
    install_requires=[
        "numpy",
        "scipy",
        "matplotlib",
    ]
)
