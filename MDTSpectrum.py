from __future__ import annotations

import array
import base64
import collections
import copy
from datetime import datetime
from struct import unpack
import os
import numpy as np
from scipy.signal import savgol_filter, medfilt, find_peaks


Spectrum = collections.namedtuple("Spectrum",
                                  ['bytesRead', 'intensity', 'wavenumbers', 'wavelength', 'filename', 'date'])
MDTFrame = collections.namedtuple("Frame",
                                  ['size', 'type', 'version', 'year', 'month', 'day', 'hour', 'minute', 'second',
                                   'padding'])

class MDTSpectrum:
    M_D = None
    baseline = None

    def __init__(self, read: Spectrum, filename: str):
        self.intensity = np.array(read.intensity)
        self.wavenumbers = np.array(read.wavenumbers)
        self.wavelength = np.array(read.wavelength)
        self.samplename = read.filename
        self.date = read.date
        self.filename = filename

    def __str__(self):
        return "{}({})".format(self.samplename, self.date)

    def __sub__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            BGS = copy.deepcopy(self)
            BGS.intensity = BGS.intensity - other
            return BGS
        elif isinstance(other, MDTSpectrum) and len(self.intensity) == len(other.intensity):
            BGS = copy.deepcopy(self)
            BGS.intensity = BGS.intensity - other.intensity
            return BGS
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            BGS = copy.deepcopy(self)
            BGS.intensity = BGS.intensity + other
            return BGS
        elif isinstance(other, MDTSpectrum) and len(self.intensity) == len(other.intensity):
            BGS = copy.deepcopy(self)
            BGS.intensity = BGS.intensity + other.intensity
            return BGS
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            BGS = copy.deepcopy(self)
            BGS.intensity = BGS.intensity * other
            return BGS
        else:
            return NotImplemented

    def __truediv__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            BGS = copy.deepcopy(self)
            BGS.intensity = BGS.intensity / other
            return BGS
        else:
            return NotImplemented


    def backgroundRemoved(self, lambdaVal: int = 5000, pVal: float = 0.1) -> MDTSpectrum:
        result=self.__removeBackground(self.intensity, lambdaVal=lambdaVal, pVal=pVal)
        BGS=copy.deepcopy(self)
        BGS.intensity=result[0]
        BGS.baseline=result[1]
        return BGS

    def __removeBackground(self,inVector, lambdaVal=5000, pVal=0.1, M_D=None):
        inVectorDuplicate=np.copy(inVector)

        if M_D is None or self.M_D is None:
            M_D=self.__makeMD(inVector.shape)
            self.M_D=M_D

        W_One=np.ones_like(inVector)
        M_Diagonal=np.diag(W_One)
        stopVal=3
        M_B=np.zeros_like(inVector)
        for _ in range(0, stopVal):
            L=(M_Diagonal + lambdaVal * np.transpose(M_D) @ M_D)
            weightedIn=W_One * inVectorDuplicate
            M_B=np.linalg.solve(L,weightedIn)

            W_One = np.where(inVectorDuplicate < M_B, pVal, 1-pVal)

        result=inVectorDuplicate-M_B

        return result, M_B

    def __makeMD(self, shape):
        M_D = np.zeros((shape[0] - 1, shape[0]))
        for p in range(M_D.shape[0]):  # this should work more elegantly without loops!
            for q in range(M_D.shape[1]):
                if p == q:
                    M_D[p, q] = -1
                elif p == q - 1:
                    M_D[p, q] = 1
                else:
                    M_D[p, q] = 0
        return M_D

    def smoothed(self, windowLength: int = 5, degree: int = 3) -> MDTSpectrum:
        sm = savgol_filter(self.intensity, window_length=windowLength, polyorder=degree)
        BGS = copy.deepcopy(self)
        BGS.intensity = sm
        return BGS

    def cosmicRayFiltered(self, kernel: int = 13, threshold: int = 1000, expand=1) -> MDTSpectrum:
        med = medfilt(self.intensity, kernel)
        diff = self.intensity - med
        peaks = find_peaks(diff, height=(threshold, None), width=(None, None))
        if peaks[0].shape[0] == 0:
            return self
        else:
            leftBorders = np.array(peaks[1]["left_ips"] - expand * peaks[1]["widths"], dtype=int)
            rightBorders = np.array(peaks[1]["right_ips"] + expand * peaks[1]["widths"], dtype=int)
            med_replaced = self.intensity.copy()

            for l, r in zip(leftBorders, rightBorders):
                med_replaced[l:r] = med[l:r]

            BGS = copy.deepcopy(self)
            BGS.intensity = med_replaced
            return BGS

    def save(self, path: str):
        data=np.stack((self.wavenumbers, self.intensity),1)
        np.savetxt(path, data, delimiter="\t")

    def stringData(self):
        st = ""
        for d in zip(self.wavenumbers, self.intensity):
            st += "{:f}\t{:f}\n".format(d[0], d[1])

        return st

    def htmlDownloadText(self):
        b64 =  base64.b64encode(self.stringData().encode("ascii")).decode("ascii")
        htmlText = """<a download='{}' href='data:text/csv;charset=utf-8;base64,{}'>Download</a>""".format(self.samplename+".txt", b64)
        return htmlText

    def min_WN(self) -> float:
        return np.min(self.wavenumbers)

    def max_WN(self) -> float:
        return np.max(self.wavenumbers)



def __readSpectrum(file, position):
    headerLength = 22
    offset = position

    file.seek(0, os.SEEK_END)
    fileSize = file.tell()
    file.seek(position)

    if headerLength > fileSize - offset:
        return Spectrum(-1, 0, 0, 0, 0, 0)

    frameBuffer = file.read(headerLength)
    frameStr = '=ihhhhhhhhh'
    frame = MDTFrame._make(unpack(frameStr, frameBuffer))

    if frame.size + offset > fileSize:
        return Spectrum(-1, 0, 0, 0, 0, 0)

    position = headerLength + frame.padding + offset + 2
    file.seek(position)
    length = unpack('=h', file.read(2))[0]
    position += 6
    file.seek(position)
    xValues = array.array('f')
    xValues.fromfile(file, length)

    position += length * 4
    file.seek(position)
    yValues = array.array('f')
    yValues.fromfile(file, length)

    position += length * 4
    file.seek(position)

    titleSize, = unpack('=i', file.read(4))
    position += 4

    titleArray = array.array('b')
    titleArray.fromfile(file, titleSize)
    title = titleArray.tobytes().decode('ascii')

    position += titleSize

    file.seek(position)
    bytesLeft = frame.size + offset - position

    if bytesLeft < 0:
        bytesLeft = (fileSize - position)

    xmlStringArray = array.array('b')
    xmlStringArray.fromfile(file, bytesLeft)
    xmlArray = xmlStringArray.tobytes()
    xml = xmlArray.decode('utf-16', errors='replace')
    wlStartString = '<SWLaserWL>'
    wlStart = xml.find(wlStartString)
    wlEnd = xml.find('</SWLaserWL>')
    waveLength = 1

    if wlStart > 0 and wlEnd > 0:
        wlString = xml[wlStart + len(wlStartString):wlEnd]
        waveLength = float(wlString)
        waveNumbers = list(map(lambda x: (1 / waveLength - 1 / x) * 1e7, xValues))
    else:
        waveNumbers = []

    date = datetime(year=frame.year, month=frame.month, day=frame.day, hour=frame.hour, minute=frame.minute, second=frame.second)
    return Spectrum(frame.size, yValues, waveNumbers, xValues, title, date)

def __readSpectrum_buffer(bytes, position):
    headerLength = 22
    offset = position


    fileSize = len(bytes)


    if headerLength > fileSize - offset:
        return Spectrum(-1, 0, 0, 0, 0, 0)

    frameBuffer = bytes[position : position+headerLength]
    frameStr = '=ihhhhhhhhh'
    frame = MDTFrame._make(unpack(frameStr, frameBuffer))

    if frame.size + offset > fileSize:
        return Spectrum(-1, 0, 0, 0, 0, 0)

    position = headerLength + frame.padding + offset + 2
    lengthBuffer=bytes[position:position+2]


    length = unpack('=h', lengthBuffer)[0]
    position += 6

    xValues = array.array('f')
    xValues.frombytes(bytes[position:position+length*4])
    position += length * 4


    yValues = array.array('f')
    yValues.frombytes(bytes[position:position+length*4])

    position += length * 4


    titleSize, = unpack('=i', bytes[position:position+4])
    position += 4

    title = bytes[position:position+titleSize].decode("ascii")

    position += titleSize

    bytesLeft = frame.size + offset - position

    if bytesLeft < 0:
        bytesLeft = (fileSize - position)


    xml = bytes[position:position+bytesLeft].decode('utf-16', errors='replace')

    wlStartString = '<SWLaserWL>'
    wlStart = xml.find(wlStartString)
    wlEnd = xml.find('</SWLaserWL>')
    waveLength = 1

    if wlStart > 0 and wlEnd > 0:
        wlString = xml[wlStart + len(wlStartString):wlEnd]
        waveLength = float(wlString)
        waveNumbers = list(map(lambda x: (1 / waveLength - 1 / x) * 1e7, xValues))
    else:
        waveNumbers = []

    date = datetime(year=frame.year, month=frame.month, day=frame.day, hour=frame.hour, minute=frame.minute, second=frame.second)
    return Spectrum(frame.size, yValues, waveNumbers, xValues, title, date)


def openMDT(filePath):
    file = open(filePath, "rb")
    # print(file.name)

    bytesRead = 0
    position = 33
    spectra = []
    while bytesRead >= 0:
        r = __readSpectrum(file, position)
        if r.bytesRead > 0:
            spectra.append(MDTSpectrum(read=r, filename=file.name))
        bytesRead = r.bytesRead
        position += bytesRead

    file.close()
    return spectra

def readMDT(bytes, filename = "Data"):
    bytesRead = 0
    position = 33
    spectra = []
    while bytesRead >= 0:
        r = __readSpectrum_buffer(bytes=bytes, position=position)
        if r.bytesRead > 0:
            spectra.append(MDTSpectrum(read=r, filename=filename))
        bytesRead = r.bytesRead
        position += bytesRead

    return spectra




def __testBytes(filePath):
    file = open(filePath, "rb")
    with file:
        bytes=file.read()
        return readMDT(bytes)



# spectra=readMDT("data.mdt")

# spectra = __testBytes("data.mdt")
# BG=spectra[0].backgroundRemoved().smoothed().cosmicRayFiltered()
# BGG = spectra[0] + spectra[1]
# print(spectra[0].htmlDownloadText())